# Gatsby Tutorial

This is the execution of the [Gatsby Tutorial](https://www.gatsbyjs.com/docs/tutorial/). It is hosted on GitLab pages (deployed via CI/CD pipeline) at https://gatsby.oliverrobson.tech/.

## Requirements

- [Node](https://nodejs.org/en/) 14+
- [Gatsby](https://www.gatsbyjs.com/) 13+
  - `npm install --global gatsby`

## Use

1. Clone the repo
2. Enter repo: `cd gatsby-tute`
3. Install JavaScript dependencies: `npm install`
4. Run the development server: `gatsby develop`
5. Navigate to `localhost:8000` and the site should appear!

## Useful Gatsby Links

  - [Documentation](https://www.gatsbyjs.com/docs/)

  - [Tutorials](https://www.gatsbyjs.com/tutorial/)

  - [Guides](https://www.gatsbyjs.com/tutorial/)

  - [API Reference](https://www.gatsbyjs.com/docs/api-reference/)

  - [Plugin Library](https://www.gatsbyjs.com/plugins)

  - [Cheat Sheet](https://www.gatsbyjs.com/docs/cheat-sheet/)
